# symfony/security-guard

Brings many layers of authentication together. https://symfony.com/doc/current/components/security

# Deprecated
* [*New in Symfony 5.3: Guard Component Deprecation*
  ](https://symfony.com/blog/new-in-symfony-5-3-guard-component-deprecation)

# Unofficial documentation
## Tuturials
* [*Basic Authentication and Registration Steps with Symfony Security Bundle (Symfony 5)*
  ](https://medium.com/suleyman-aydoslu/basic-authentication-and-registration-steps-with-symfony-security-bundle-symfony-5-a5518ee0a9da)
  2020-05 Süleyman Aydoslu
* [*Decoupling your application User entity from Symfony’s security*](https://medium.com/@simshaun/decoupling-your-application-user-from-symfonys-security-user-60fa31b4f7f2)
  2018-11 Shaun Simmons
